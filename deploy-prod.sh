#!/bin/sh
gradle build
echo "Copying application.yml to s3"
aws s3 cp application.yml s3://it.yonder.deploy/workers/wadi/  
echo "Copying wadi.cron to s3"
aws s3 cp wadi.cron s3://it.yonder.deploy/workers/wadi/
echo "Building wadi.sh to build/libs/wadi.sh"  
shfile="#!/bin/sh\ncd /var/lib/apps/wadi;\njava -jar wadi.jar --spring.profiles.active=production;"  
echo $shfile > build/libs/wadi.sh 
echo "Copying build/libs/wadi.sh to s3"
aws s3 cp build/libs/wadi.sh s3://it.yonder.deploy/workers/wadi/  
echo "Copying build/libs/wadi.jar to s3"
aws s3 cp build/libs/wadi.jar s3://it.yonder.deploy/workers/wadi/ 
echo "Copying aws-install.sh to s3"
aws s3 cp aws-install.sh s3://it.yonder.deploy/workers/wadi/aws-wadi-install.sh 
echo "Deployment Complete"