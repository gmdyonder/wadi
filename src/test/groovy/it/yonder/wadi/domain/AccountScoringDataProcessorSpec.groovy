package it.yonder.wadi.domain

import it.yonder.wadi.processor.AccountScoringDataProcessor
import spock.lang.Specification

/**
 * User: rmorgan
 * Date: 11/21/14
 * Time: 5:38 PM
 */
class AccountScoringDataProcessorSpec extends Specification {

	def "the processor should transform data into a clout object"(){
		given: "a processor"
			AccountScoringDataProcessor processor = new AccountScoringDataProcessor()
		and: "a data object"
			AccountScoringData mockData = Mock()
			mockData.getAccountId() >> accountId
			mockData.getCloutId() >> cloutId
			mockData.getCreatorScore() >> cScore
			mockData.getSpectatorScore() >> sScore
		when: "the data object is processed"
			Clout clout = processor.process(mockData)
		then: "the resulting clout object has the expected values"
			clout.accountId == accountId
			clout.id == cloutId
			clout.creatorScore == cScore
			clout.spectatorScore == sScore
		where:
			cloutId	|	accountId	|	cScore	|	sScore
			null		|	3					| 34			|	12
			9				|	5					| 64			|	92
	}
}
