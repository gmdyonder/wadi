package it.yonder.wadi.domain

import com.mysql.jdbc.JDBC4ResultSet
import it.yonder.wadi.mapper.AccountScoringDataRowMapper
import spock.lang.Specification

import java.sql.ResultSet

/**
 * User: rmorgan
 * Date: 11/21/14
 * Time: 5:39 PM
 */
class AccountScoringDataRowMapperSpec extends Specification {
	def "given a result set, account scoring data will be populated properly"(){
		given: "a result set"
			Date ofBatchJob = new Date()
			JDBC4ResultSet rs = Mock()
			rs.getLong("id") >> 4
			rs.getLong("cloutId") >> 5
			rs.getLong("batchJobStartTime") >> ofBatchJob.time
			rs.getBigDecimal("spectatorScore1") >> 3.14
			rs.getBigDecimal("spectatorScore2") >> 23.099
			rs.getBigDecimal("spectatorScore3") >> 0.0
			rs.getBigDecimal("creatorScore1") >> 34.1
			rs.getBigDecimal("creatorScore2") >> 20.1234
			rs.getBigDecimal("creatorScore3") >> 12.3453
			rs.getBigDecimal("creatorScore4") >> 19.89
			rs.getDate("lastPostDate") >> new java.sql.Date((ofBatchJob - 3).time)
		and: "a row mapper"
			AccountScoringDataRowMapper mapper = new AccountScoringDataRowMapper()
		when: "the result set is mapped"
			def result = mapper.mapRow(rs, 1)
		then: "the expected account data is provided"
			result.accountId == 4
			result.cloutId == 5
			result.batchJobStartTime == ofBatchJob.time
			result.likesPerNewExperiencesScore == 3.14
			result.commentsPerNewExperiencesScore == 23.099
			result.newFollowingsRatioScore == 0.0
			result.likesVsFollowerScore == 34.1
			result.likesPerPostScore == 20.1234
			result.postingFrequencyScore == 12.3453
			result.starPowerScore == 19.89
			result.lastPost.time == (ofBatchJob-3).time

	}
}
