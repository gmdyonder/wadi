package it.yonder.wadi.domain

import spock.lang.Specification
import spock.lang.Unroll

/**
 * User: rmorgan
 * Date: 11/18/14
 * Time: 5:13 PM
 */
class AccountScoringDataSpec extends Specification{

	@Unroll
	def "spectator score should be a sum of likesPer, commentsPer and followings scores"(){
		given:
			def accountScoringData = new AccountScoringData(
					likesPerNewExperiencesScore: likesScore,
					commentsPerNewExperiencesScore: commentsScore,
					newFollowingsRatioScore: ratioScore
			)
		when:
			def score = accountScoringData.getSpectatorScore()
		then:
			score == likesScore + commentsScore + ratioScore
		where:
			likesScore	| commentsScore	| ratioScore
			0						| 0							| 0
			0						| 3.4						| 5.7
			1.2					| 0						  | 5.7
			1.2					| 3.4						| 0
			1.2					| 3.4						| 5.7
	}

	@Unroll
	def "creator score should be a sum of posting scores"(){
		given:
			AccountScoringData accountScoringData = Spy(){
				getLastPostScore() >> {
					score5
				}
			}
			accountScoringData.batchJobStartTime = new Date().time
			accountScoringData.postingFrequencyScore = score1
			accountScoringData.likesPerPostScore = score2
			accountScoringData.starPowerScore = score3
			accountScoringData.likesVsFollowerScore = score4
		when:
			def score = accountScoringData.getCreatorScore()
		then:
			score == score1 + score2 + score3 + score4 + score5
		where:
			score1	| score2	| score3	| score4	|score5
			0				| 0				| 0				| 0				| 0
			0				| 3.4			| 5.7			| 3.4			| 5.7
			1.2			| 0				| 5.7			| 0				| 5.7
			1.2			| 3.4			| 0				| 3.4			| 0
			1.2			| 3.4			| 5.7			| 3.4			| 5.7
	}

	@Unroll
	def "calculating last post score is based on the difference between run time and the last post date"(){
		given:
			AccountScoringData data = new AccountScoringData(
					batchJobStartTime: Date.parse("d/M/yyyy H:m:s", startTime).time,
					lastPost: new java.sql.Date(Date.parse("d/M/yyyy H:m:s", lastPost).time)
			)
		when:
			def result = data.getLastPostScore()
		then:
			result.toString().startsWith(expected)
		where:
			lastPost							| startTime							| expected //these expected scores are based on weight of .15
			"31/10/2014 16:02:43" | "31/10/2014 16:02:43" | "15"
			"31/10/2014 04:02:43" | "31/10/2014 16:02:43" | "15"
			"30/10/2014 04:01:43" | "31/10/2014 16:02:43" | "13.67"
			"20/10/2014 16:02:43" | "31/10/2014 16:02:43" | "7.1"
			"31/01/2013 16:02:43" | "31/10/2014 16:02:43" | "0"
	}
}
