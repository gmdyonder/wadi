package it.yonder.wadi.domain

import it.yonder.wadi.configuration.CloutClassifier
import it.yonder.wadi.configuration.CloutInsertItemWriter
import it.yonder.wadi.configuration.CloutUpdateItemWriter
import org.springframework.batch.item.ItemWriter
import spock.lang.Specification

import javax.sql.DataSource

/**
 * User: rmorgan
 * Date: 11/21/14
 * Time: 5:43 PM
 */
class CloutClassifierSpec extends Specification {
	def "the classifier will provide the appropriate writer based on clout id"(){
		given:
			CloutClassifier classifier = new CloutClassifier(
					cloutInsertItemWriter: new CloutInsertItemWriter(Mock(DataSource)),
					cloutUpdateItemWriter: new CloutUpdateItemWriter(Mock(DataSource))
			)
		when:
			ItemWriter writer = classifier.classify(new Clout(id: cloutId))
		then:
			writer == (expectedType == "update" ? classifier.cloutUpdateItemWriter : classifier.cloutInsertItemWriter)
		where:
			cloutId	|	expectedType
			null		|	"insert"
			1				|	"update"

	}
}
