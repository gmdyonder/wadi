package it.yonder.wadi.domain

import it.yonder.wadi.configuration.CloutClassifier
import it.yonder.wadi.configuration.CloutInsertItemWriter
import it.yonder.wadi.configuration.CloutUpdateItemWriter
import it.yonder.wadi.configuration.UserScoringBatchConfiguration
import it.yonder.wadi.mapper.AccountScoringDataRowMapper
import it.yonder.wadi.processor.AccountScoringDataProcessor
import org.springframework.batch.core.Job
import org.springframework.batch.core.Step
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory
import org.springframework.batch.core.job.builder.FlowJobBuilder
import org.springframework.batch.core.job.builder.JobBuilder
import org.springframework.batch.core.job.builder.JobFlowBuilder
import org.springframework.batch.core.step.builder.SimpleStepBuilder
import org.springframework.batch.core.step.builder.StepBuilder
import org.springframework.batch.core.step.tasklet.TaskletStep
import org.springframework.batch.item.ItemProcessor
import org.springframework.batch.item.ItemReader
import org.springframework.batch.item.ItemWriter
import org.springframework.batch.item.database.Order
import org.springframework.batch.item.database.support.MySqlPagingQueryProvider
import org.springframework.batch.item.support.ClassifierCompositeItemWriter
import org.springframework.jdbc.core.ColumnMapRowMapper
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import spock.lang.Specification

import javax.sql.DataSource

import static it.yonder.wadi.ScoringConstants.*
import static it.yonder.wadi.ScoringConstants.LIKES_PER_EXP_DIVISOR_PARAM
import static it.yonder.wadi.ScoringConstants.LIKES_PER_FOLLOWER_DIVISOR_PARAM
import static it.yonder.wadi.ScoringConstants.NUM_EXP_DIVISOR_PARAM
import static it.yonder.wadi.ScoringConstants.STAR_POWER_DIVISOR_PARAM

/**
 * User: rmorgan
 * Date: 11/21/14
 * Time: 5:40 PM
 */
class UserScoringBatchConfigurationSpec extends Specification{
	UserScoringBatchConfiguration config

	def setup(){
		config = new UserScoringBatchConfiguration()
	}

	def "the item reader should call getDivisorFor for each divisor query"(){
		given: "a data source"
			DataSource dataSource = Mock()
			UserScoringBatchConfiguration spy = Spy()
		when: "the reader is retrieved"
			def reader = spy.reader(dataSource)
		then:
			1 * spy.getDivisorFor(_,NUM_LIKES_DIVISOR_QUERY, _, _) >> 2
			1 * spy.getDivisorFor(_,NUM_COMMENTS_DIVISOR_QUERY, _, _) >> 2
			1 * spy.getDivisorFor(_,LIKES_PER_FOLLOWER_DIVISOR_QUERY, _, _) >> 2
			1 * spy.getDivisorFor(_,LIKES_PER_EXP_DIVISOR_QUERY, _, _) >> 2
			1 * spy.getDivisorFor(_,NUM_EXP_DIVISOR_QUERY, _, _) >> 2
			1 * spy.getDivisorFor(_,STAR_POWER_DIVISOR_QUERY, _, _) >> 2
//			6 * spy.getDivisorFor(_,_,_,_) >> 2
	}

	def "getDivisorFor should execute the provided query and return the log of the result"(){
		given: "a jdbc template"
			NamedParameterJdbcTemplate template = Mock()
		when: "a divisor is requested"
			def divisor = config.getDivisorFor(template, NUM_LIKES_DIVISOR_QUERY, new Date() -31, new Date())
		then:
			1 * template.query(NUM_LIKES_DIVISOR_QUERY, _, _) >> { query, map, mapper ->
				assert map[START_DATE_PARAM] != null
				assert map[END_DATE_PARAM] != null
				return [[result: max]]
			}
			divisor == expected
		where:
			max		|	expected
			100		| 2
			1000	| 3
	}

	def "when no result is returned from a getDivisorFor query, an exception occurs"(){
		given: "a jdbc template"
			NamedParameterJdbcTemplate template = Mock()
			1 * template.query(NUM_LIKES_DIVISOR_QUERY, _, _) >> { query, map, mapper ->
				assert map[START_DATE_PARAM] != null
				assert map[END_DATE_PARAM] != null
				return []
			}
		when: "a divisor is requested"
			config.getDivisorFor(template, NUM_LIKES_DIVISOR_QUERY, new Date() -31, new Date())
		then:
			def e = thrown(Exception)
			e.message == "Num likes divisor query did not return a result."
	}

	def "step1 should utilize the factory provided appropriately"(){
		given:
			StepBuilderFactory stepBuilderFactory = Mock()
			ItemReader<AccountScoringData> reader = Mock()
			ItemWriter<Clout> writer = Mock()
			ItemProcessor<AccountScoringData, Clout> processor = Mock()
			StepBuilder stepBuilder = Mock()
			SimpleStepBuilder simpleStepBuilder = Mock()
			TaskletStep step = Mock()
		when:
			Step s = config.step1(stepBuilderFactory, reader, writer, processor)
		then:
			1 * stepBuilderFactory.get("step1") >> stepBuilder
			1 * stepBuilder.chunk(500) >> simpleStepBuilder
			1 * simpleStepBuilder.reader(reader) >> simpleStepBuilder
			1 * simpleStepBuilder.processor(processor) >> simpleStepBuilder
			1 * simpleStepBuilder.writer(writer) >> simpleStepBuilder
			1 * simpleStepBuilder.build() >> step
			s == step

	}

	def "cloutScoringJob should utilize the factory provided appropriately"(){
		given: "the jobs factory and step"
			JobBuilderFactory jobs = Mock()
			JobBuilder jobBuilder = Mock()
			JobFlowBuilder flowBuilder = Mock()
			FlowJobBuilder flowJobBuilder = Mock()
			Job job = Mock()
			Step s1 = Mock()
		when: "the job is retrieved"
			def result = config.cloutScoringJob(jobs, s1)
		then:
			1 * jobs.get("cloutScoringJob") >> jobBuilder
			1 * jobBuilder.incrementer(_) >> jobBuilder
			1 * jobBuilder.flow(s1) >> flowBuilder
			1 * flowBuilder.build() >> flowJobBuilder
			1 * flowJobBuilder.build() >> job
			result == job

	}

	def "the writer bean should be the expected type"(){
		given: "a data source"
			DataSource dataSource = Mock()
		when: "the writer is retrieved"
			def writer = config.writer(dataSource)
		then: "the processor is AccountScoringDataProcessor"
			writer instanceof ClassifierCompositeItemWriter<Clout>
			writer.classifier instanceof CloutClassifier
			writer.classifier.cloutInsertItemWriter instanceof CloutInsertItemWriter
			writer.classifier.cloutUpdateItemWriter instanceof CloutUpdateItemWriter
	}

	def "processor should provide the expected process type"(){
		when: "the processor is retrieved"
			def processor = config.processor()
		then: "the processor is an AccountScoringDataProcessor"
			processor instanceof AccountScoringDataProcessor
	}

	def "the item reader should be a paging item reader"(){
		given: "a data source"
			DataSource dataSource = Mock()
			config.USE_CONSTANT_SCORING_DIVISORS = true
		when: "the reader is retrieved"
			def reader = config.reader(dataSource)
		then: ""
			reader.properties.rowMapper instanceof AccountScoringDataRowMapper
			reader.properties.dataSource == dataSource
			reader.properties.queryProvider instanceof MySqlPagingQueryProvider

	}

	def "the item reader should use a MySqlPagingQuery provider with the appropriate config"(){
		given: "a data source"
			DataSource dataSource = Mock()
			config.USE_CONSTANT_SCORING_DIVISORS = true
		when: "the reader is retrieved"
			def reader = config.reader(dataSource)
		then: "the expected parameters are present"
			reader.properties.queryProvider instanceof MySqlPagingQueryProvider
			reader.properties.queryProvider.properties.selectClause.trim() == SCORING_SELECT_STATEMENT.trim()
			reader.properties.queryProvider.properties.fromClause.trim() == SCORING_FROM_CLAUSE.trim()
			reader.properties.queryProvider.properties.sortKeys.id == Order.ASCENDING
	}

	def "the item reader should contain the expected parameter values"(){
		given: "a data source"
			DataSource dataSource = Mock()
			config.USE_CONSTANT_SCORING_DIVISORS = true
		when: "the reader is retrieved"
			def reader = config.reader(dataSource)
		then: "the expected parameters are present"
		reader.properties.parameterValues.size() == 8
		reader.properties.parameterValues.keySet().containsAll(
				["startDate","endDate",
				 "${NUM_LIKES_DIVISOR_PARAM}".toString(),
				 "${NUM_COMMENTS_DIVISOR_PARAM}".toString(),
				 "${LIKES_PER_FOLLOWER_DIVISOR_PARAM}".toString(),
				 "${LIKES_PER_EXP_DIVISOR_PARAM}".toString(),
				 "${NUM_EXP_DIVISOR_PARAM}".toString(),
				 "${STAR_POWER_DIVISOR_PARAM}".toString()
				]
		)

	}

}
