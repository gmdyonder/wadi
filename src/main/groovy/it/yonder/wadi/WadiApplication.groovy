package it.yonder.wadi

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.ComponentScan

import javax.sql.DataSource

/**
 * This application runs a batch job on start up for calculating scores for users based on their social engagement in Yonder.
 * User: rmorgan
 * Date: 11/18/14
 * Time: 2:22 PM
 */
@EnableAutoConfiguration
@ComponentScan("it.yonder.wadi")
class WadiApplication {
	public static void main(String[] args){
		ApplicationContext ctx = SpringApplication.run(WadiApplication, args)
	}
}
