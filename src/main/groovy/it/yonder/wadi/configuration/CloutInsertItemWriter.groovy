package it.yonder.wadi.configuration

import it.yonder.wadi.ScoringConstants
import it.yonder.wadi.domain.Clout
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider
import org.springframework.batch.item.database.JdbcBatchItemWriter
import javax.sql.DataSource

/**
 * A JdbcBatchItemWriter that inserts Clout records to the supplied DataSource.
 * User: rmorgan
 * Date: 11/20/14
 * Time: 1:41 PM
 */
class CloutInsertItemWriter extends JdbcBatchItemWriter<Clout> {

	CloutInsertItemWriter(DataSource dataSource){
		sql = ScoringConstants.CLOUT_INSERT_STATEMENT
		setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Clout>())
		setDataSource(dataSource)
		this.afterPropertiesSet()
	}
}
