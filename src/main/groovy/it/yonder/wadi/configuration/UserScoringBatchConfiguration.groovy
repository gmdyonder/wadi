package it.yonder.wadi.configuration

import it.yonder.wadi.domain.AccountScoringData
import it.yonder.wadi.domain.Clout
import it.yonder.wadi.mapper.AccountScoringDataRowMapper
import it.yonder.wadi.processor.AccountScoringDataProcessor
import org.springframework.batch.core.Job
import org.springframework.batch.core.Step
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory
import org.springframework.batch.core.launch.support.RunIdIncrementer
import org.springframework.batch.item.ItemProcessor
import org.springframework.batch.item.ItemReader
import org.springframework.batch.item.ItemWriter
import org.springframework.batch.item.database.JdbcPagingItemReader
import org.springframework.batch.item.database.Order
import org.springframework.batch.item.database.support.MySqlPagingQueryProvider
import org.springframework.batch.item.support.ClassifierCompositeItemWriter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.jdbc.core.ColumnMapRowMapper
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import static it.yonder.wadi.ScoringConstants.*

import javax.sql.DataSource


/**
 * This Configuration class holds all of the configuration for the Spring Batch job to reader AccountScoringData and
 * write the summation of that data as Clout records.
 * User: rmorgan
 * Date: 11/18/14
 * Time: 3:13 PM
 */
@Configuration
@EnableBatchProcessing
class UserScoringBatchConfiguration {
	public static boolean USE_CONSTANT_SCORING_DIVISORS = false;
	@Autowired
	public DataSourceProperties properties;

	@Bean
	public DataSource dataSource(){
		System.out.println "------------- Connecting to ${properties.url} -------------"
		DataSource dataSource = DataSourceBuilder.create()
				.url(properties.url)
				.driverClassName(properties.driverClassName)
				.username(properties.username)
				.password(properties.password)
				.build()
		System.out.println "------------- Connected -------------"
		return dataSource
	}

	// tag::readerwriterprocessor[]
	/**
	 * This reader is autowired as a bean. It's job is to first get the divisors to be used as parameters in the scoring
	 * queries and then return the reader to be used by the batch job.
	 * @param dataSource
	 * @return JdbcPagingItemReader reader
	 */
	@Bean
	public ItemReader<AccountScoringData> reader(DataSource dataSource) {
		def template = new NamedParameterJdbcTemplate(dataSource);
		def startDate = new Date() - 31
		def endDate = new Date()
		MySqlPagingQueryProvider pagingQueryProvider = new MySqlPagingQueryProvider()
		pagingQueryProvider.setSelectClause(SCORING_SELECT_STATEMENT)
		pagingQueryProvider.setFromClause(SCORING_FROM_CLAUSE)
		pagingQueryProvider.setSortKeys("id": Order.ASCENDING)

		JdbcPagingItemReader<AccountScoringData> reader = new JdbcPagingItemReader<AccountScoringData>()
		reader.setRowMapper(new AccountScoringDataRowMapper())
		reader.setDataSource(dataSource)
		reader.setQueryProvider(pagingQueryProvider)
		//not sure if these are working right now
		Map params = [:]
		params.put("startDate", startDate)
		params.put("endDate", endDate)
		if(USE_CONSTANT_SCORING_DIVISORS){
			println "WARNING - USING CONSTANT DIVISORS. SCORES WILL NOT BE ACCURATE."
			params.put("${NUM_LIKES_DIVISOR_PARAM}".toString(), 2)
			params.put("${NUM_COMMENTS_DIVISOR_PARAM}".toString(), 2)
			params.put("${LIKES_PER_FOLLOWER_DIVISOR_PARAM}".toString(), 2)
			params.put("${LIKES_PER_EXP_DIVISOR_PARAM}".toString(), 2)
			params.put("${NUM_EXP_DIVISOR_PARAM}".toString(), 2)
			params.put("${STAR_POWER_DIVISOR_PARAM}".toString(), 2)
		} else {
			params.put("${NUM_LIKES_DIVISOR_PARAM}".toString(), getDivisorFor(template, NUM_LIKES_DIVISOR_QUERY, startDate, endDate))
			params.put("${NUM_COMMENTS_DIVISOR_PARAM}".toString(), getDivisorFor(template, NUM_COMMENTS_DIVISOR_QUERY, startDate, endDate))
			params.put("${LIKES_PER_FOLLOWER_DIVISOR_PARAM}".toString(), getDivisorFor(template, LIKES_PER_FOLLOWER_DIVISOR_QUERY, startDate, endDate))
			params.put("${LIKES_PER_EXP_DIVISOR_PARAM}".toString(), getDivisorFor(template, LIKES_PER_EXP_DIVISOR_QUERY, startDate, endDate))
			params.put("${NUM_EXP_DIVISOR_PARAM}".toString(), getDivisorFor(template, NUM_EXP_DIVISOR_QUERY, startDate, endDate))
			params.put("${STAR_POWER_DIVISOR_PARAM}".toString(), getDivisorFor(template, STAR_POWER_DIVISOR_QUERY, startDate, endDate))
		}

		reader.setParameterValues(params)
		reader.afterPropertiesSet()
		return reader
	}
	/**
	 * A simple ItemProcessor to transform an AccountScoringData item to a Clout item
	 * @return
	 */
	@Bean
	public ItemProcessor<AccountScoringData, Clout> processor() {
		return new AccountScoringDataProcessor();
	}

	/**
	 * The writer that will insert/update Clout data
	 * @param dataSource
	 * @return ClassifierCompositeItemWriter writer
	 */
	@Bean
	public ItemWriter<Clout> writer(DataSource dataSource) {
		ClassifierCompositeItemWriter<Clout> writer = new ClassifierCompositeItemWriter<Clout>(classifier: new CloutClassifier(
				cloutUpdateItemWriter: new CloutUpdateItemWriter(dataSource),
				cloutInsertItemWriter: new CloutInsertItemWriter(dataSource)
		));
		return writer;
	}

	// end::readerwriterprocessor[]

	// tag::jobstep[]
	/**
	 * Defines the Clout Scoring Job
	 * @param jobs
	 * @param s1
	 * @return
	 */
	@Bean
	public Job cloutScoringJob(JobBuilderFactory jobs, Step s1) {
		return jobs.get("cloutScoringJob")
				.incrementer(new RunIdIncrementer())
				.flow(s1)
				.end()
				.build();
	}

	/**
	 * The one and only step in this job is to read the AccountScoringData and write the results Clout data. All of the
	 * inputs are also wired as beans, so that Spring can simply execute the job and step on start up of the app.
	 *
	 * @param stepBuilderFactory
	 * @param reader
	 * @param writer
	 * @param processor
	 * @return
	 */
	@Bean
	public Step step1(StepBuilderFactory stepBuilderFactory, ItemReader<AccountScoringData> reader,
										ItemWriter<Clout> writer, ItemProcessor<AccountScoringData, Clout> processor) {
		return stepBuilderFactory.get("step1")
				.<AccountScoringData, Clout> chunk(500)
				.reader(reader)
				.processor(processor)
				.writer(writer)
				.build();
	}
	// end::jobstep[]

	/**
	 * Compute the divisor for a logarithm such that log10() of the query divided by the result of this function equals 1
	 * @param template
	 * @param query
	 * @param start
	 * @param end
	 * @return the divisor that given a log(10) of the query is equal to 1
	 */
	public BigDecimal getDivisorFor(NamedParameterJdbcTemplate template, String query, Date start, Date end){
		Map params = [:]
		params.put("${START_DATE_PARAM}".toString(), start)
		params.put("${END_DATE_PARAM}".toString(), end)
		List rows = template.query(query, params, new ColumnMapRowMapper())
		if(rows.size() > 1 || !rows[0]){
			throw new Exception("Num likes divisor query did not return a result.")
		}
		if(!rows[0].result){
			return 1
		}

		def div = Math.log10(rows[0].result) as BigDecimal

		return div > 0 ? div : 1
	}
}
