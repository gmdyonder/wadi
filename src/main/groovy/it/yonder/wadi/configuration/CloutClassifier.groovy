package it.yonder.wadi.configuration

import it.yonder.wadi.domain.Clout
import org.springframework.batch.item.ItemWriter
import org.springframework.batch.item.database.JdbcBatchItemWriter
import org.springframework.batch.item.support.ClassifierCompositeItemWriter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.classify.Classifier

import javax.sql.DataSource

/**
 * This Classifier uses either an update or insert ItemWriter based on whether or not the Clout has an id specified.
 * User: rmorgan
 * Date: 11/20/14
 * Time: 1:35 PM
 */
class CloutClassifier implements Classifier<Clout, JdbcBatchItemWriter<Clout>>{
	CloutUpdateItemWriter cloutUpdateItemWriter
	CloutInsertItemWriter cloutInsertItemWriter

	/**
	 * Check if Clout id is null and either return the writer that inserts or the one that updates
	 * @param classifiable
	 * @return
	 */
	@Override
	JdbcBatchItemWriter<Clout> classify(Clout classifiable) {
		if(classifiable.id){
			return cloutUpdateItemWriter
		} else {
			return cloutInsertItemWriter
		}
	}
}
