package it.yonder.wadi.configuration

import it.yonder.wadi.ScoringConstants
import it.yonder.wadi.domain.Clout
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider
import org.springframework.batch.item.database.JdbcBatchItemWriter
import javax.sql.DataSource

/**
 * A JdbcBatchItemWriter that updates Clout records (based on Clout.id) in the supplied DataSource.
 * User: rmorgan
 * Date: 11/20/14
 * Time: 1:41 PM
 */
class CloutUpdateItemWriter extends JdbcBatchItemWriter<Clout> {

	CloutUpdateItemWriter(DataSource dataSource){
		sql = ScoringConstants.CLOUT_UPDATE_STATEMENT
		setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Clout>())
		setDataSource(dataSource)
		this.afterPropertiesSet()
	}
}
