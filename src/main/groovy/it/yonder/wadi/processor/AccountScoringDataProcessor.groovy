package it.yonder.wadi.processor

import it.yonder.wadi.domain.AccountScoringData
import it.yonder.wadi.domain.Clout
import org.springframework.batch.item.ItemProcessor


/**
 * This processor simply converts an AccountScoringDataItem into a Clout item
 * User: rmorgan
 * Date: 11/18/14
 * Time: 9:45 PM
 */
class AccountScoringDataProcessor implements ItemProcessor<AccountScoringData, Clout>{
	/**
	 * Process the item by getting the identifiers from it as well as the summed scores.
	 * @param item
	 * @return
	 * @throws Exception
	 */
	@Override
	Clout process(AccountScoringData item) throws Exception {
		Clout clout = new Clout(
				id: item.cloutId,
				accountId: item.accountId,
				creatorScore: item.getCreatorScore(),
				spectatorScore: item.getSpectatorScore(),
		)
		return clout
	}
}
