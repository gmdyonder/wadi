package it.yonder.wadi.mapper

import it.yonder.wadi.domain.AccountScoringData
import org.springframework.jdbc.core.RowMapper

import java.sql.ResultSet
import java.sql.SQLException

/**
 * Simply maps the SQL values from the Account Scoring Data select query to the object.
 *
 * User: rmorgan
 * Date: 11/19/14
 * Time: 10:43 AM
 */
class AccountScoringDataRowMapper implements RowMapper<AccountScoringData>{
	@Override
	AccountScoringData mapRow(ResultSet rs, int rowNum) throws SQLException {
		new AccountScoringData(
				batchJobStartTime: rs.getLong("batchJobStartTime"),
				accountId: rs.getLong("id"),
				cloutId: rs.getLong("cloutId"),
				likesPerNewExperiencesScore: rs.getBigDecimal("spectatorScore1"),
				commentsPerNewExperiencesScore: rs.getBigDecimal("spectatorScore2"),
				newFollowingsRatioScore: rs.getBigDecimal("spectatorScore3"),
				likesVsFollowerScore: rs.getBigDecimal("creatorScore1"),
				likesPerPostScore: rs.getBigDecimal("creatorScore2"),
				postingFrequencyScore: rs.getBigDecimal("creatorScore3"),
				starPowerScore: rs.getBigDecimal("creatorScore4"),
				lastPost: rs.getDate("lastPostDate")
		)
	}
}
