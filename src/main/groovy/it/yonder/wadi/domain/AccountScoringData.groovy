package it.yonder.wadi.domain

import it.yonder.wadi.ScoringConstants

import java.sql.Date
/**
 * Class representation of mostly calculated values to sum as the scores for an Account's Clout
 * User: rmorgan
 * Date: 11/18/14
 * Time: 3:55 PM
 */
class AccountScoringData {
	private static final BigDecimal MILLIS_TO_DAYS_DIVISOR = 1000 * 60 *  60 * 24
	Long batchJobStartTime
	Long accountId
	Long cloutId

	//Spectator Scoring Values
	BigDecimal likesPerNewExperiencesScore
	BigDecimal commentsPerNewExperiencesScore
	BigDecimal newFollowingsRatioScore

	//Creator Scoring Values
	BigDecimal postingFrequencyScore
	BigDecimal likesPerPostScore
	BigDecimal starPowerScore
	BigDecimal likesVsFollowerScore
	Date lastPost

	/**
	 * Runs the algorithm for calculating the score based on the Account's last posted experience date
	 * @return score between 0 and ScoringConstants.CREATOR_WEIGHT_DAYS_SINCE_LAST_POST/100
	 */
	BigDecimal getLastPostScore(){
		if(!lastPost){ return 0 }   //test short circuit
		BigDecimal time = (batchJobStartTime - lastPost.time)/MILLIS_TO_DAYS_DIVISOR
		time = time < 1 ? 1 : time
		BigDecimal score = 100 - Math.log10(Math.pow(time, 50))
		return score > 0 ? score * ScoringConstants.CREATOR_WEIGHT_DAYS_SINCE_LAST_POST/100 : 0
	}

	/**
	 * Sums the spectator score values of this AccountScoringData and returns that sum
	 * @return spectatorScore
	 */
	BigDecimal getSpectatorScore(){  //test null values
		(likesPerNewExperiencesScore > 0? likesPerNewExperiencesScore :0) + (commentsPerNewExperiencesScore > 0? commentsPerNewExperiencesScore :0) + (newFollowingsRatioScore > 0? newFollowingsRatioScore :0)
	}

	/**
	 * Sums the creator score values of this AccountScoringData and returns that sum
	 * @return creatorScore
	 */
	BigDecimal getCreatorScore(){ //test null values
		(postingFrequencyScore > 0? postingFrequencyScore:0) + (likesPerPostScore > 0? likesPerPostScore:0) + (starPowerScore > 0? starPowerScore :0) + (likesVsFollowerScore > 0 ? likesVsFollowerScore :0) + getLastPostScore()
	}

}
