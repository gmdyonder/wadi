package it.yonder.wadi

/**
 * This file contains all of the query constants and associated values.
 * User: rmorgan
 * Date: 11/20/14
 * Time: 10:43 AM
 */
class ScoringConstants {

	private static final Integer SPECTATOR_WEIGHT_NUM_LIKES = 50;
	private static final Integer SPECTATOR_WEIGHT_NUM_COMMENTS = 50;
	private static final Integer SPECTATOR_WEIGHT_FRIENDSHIP = 0;

	private static final Integer CREATOR_WEIGHT_LIKES_POWER_PER_FOLLOWER = 10;
	private static final Integer CREATOR_WEIGHT_LIKES_PER_EXPERIENCE = 30;
	private static final Integer CREATOR_WEIGHT_NUM_EXPERIENCES = 30;
	private static final Integer CREATOR_WEIGHT_STAR_POWER = 15;
	public  static final Integer CREATOR_WEIGHT_DAYS_SINCE_LAST_POST = 15;

	public static final String NUM_LIKES_DIVISOR_PARAM = "s_numLikesDivisor";
	public static final String NUM_COMMENTS_DIVISOR_PARAM = "s_numCommentsDivisor";
	public static final String LIKES_PER_FOLLOWER_DIVISOR_PARAM = "c_likesPerFollowerDivisor";
	public static final String LIKES_PER_EXP_DIVISOR_PARAM = "c_likesPerExpDivisor";
	public static final String NUM_EXP_DIVISOR_PARAM = "c_numExpDivisor";
	public static final String STAR_POWER_DIVISOR_PARAM = "c_starPowerDivisor";

	public static final String START_DATE_PARAM = "startDate"
	public static final String END_DATE_PARAM = "endDate"

	public static final String NUM_LIKES_DIVISOR_QUERY = """
select max(d.count) as result from
( select accounts_id, count(*) as count from likes
where objecttypes_id = 1 and date_created between :${START_DATE_PARAM} and :${END_DATE_PARAM} group by accounts_id) as d
"""

	public static final String NUM_COMMENTS_DIVISOR_QUERY = """
select max(d.count) as result from
( select accounts_id, count(*) as count from comments where objecttypes_id = 1 and date_created between :${START_DATE_PARAM} and :${END_DATE_PARAM} group by accounts_id) as d;
"""

	public static final String STAR_POWER_DIVISOR_QUERY = """
select max(d.ratio) from
( select a.id,
  (select count(*) as followers from friendship followers where following_accounts_id = a.id and date_created between :${START_DATE_PARAM} and :${END_DATE_PARAM})
  / (COALESCE( NULLIF((select count(*) as following from friendship following where follower_accounts_id = a.id and date_created between :${START_DATE_PARAM} and :${END_DATE_PARAM}),0),1))  as ratio
  from accounts a
) as d
"""
	public static final String NUM_EXP_DIVISOR_QUERY = """
select max(d.count) as result from (
select e.accounts_id , count(e.id) as count from experiences e where e.isactive = 1 and e.date_created between :${START_DATE_PARAM} and :${END_DATE_PARAM} group by e.accounts_id) as d;
"""
	public static final String LIKES_PER_EXP_DIVISOR_QUERY = """
select max(d.likes_per_exp) as result from
(select sum(e.like_count)/count(e.id) as likes_per_exp from experiences e where e.isactive = 1 and e.date_created between :${START_DATE_PARAM} and :${END_DATE_PARAM} group by e.accounts_id) as d;
"""
	/**
	 * like-per-experience/num-followers is intended to benefit the people with popular experiences contrast to their low number of followers
	 */
	public static final String LIKES_PER_FOLLOWER_DIVISOR_QUERY = """
  select max(d.val) from
  (select a.id, ((
    select sum(e.like_count)/count(e.id) as likes_per_exp from experiences e where e.isactive = 1 and e.date_created between :startDate and :endDate and e.accounts_id = a.id group by e.accounts_id)/
  (select count(*) + 5 as followers from friendship followers where following_accounts_id = a.id and date_created <= :endDate)) as val from accounts a) as d
"""


	public static final String SCORING_SELECT_STATEMENT =
		"""
			a.id as id, ${new Date().time} as batchJobStartTime,
			(select id from clout where account_id = a.id) as cloutId,
			LOG10((select count(*) from likes where accounts_id = a.id and objecttypes_id = 1 and date_created between :startDate and :endDate))/:${NUM_LIKES_DIVISOR_PARAM} * ${SPECTATOR_WEIGHT_NUM_LIKES} as spectatorScore1,
			LOG10((select count(*) from comments where accounts_id = a.id and objecttypes_id = 1 and date_created between :startDate and :endDate))/:${NUM_COMMENTS_DIVISOR_PARAM}  * ${SPECTATOR_WEIGHT_NUM_COMMENTS} as spectatorScore2,
			(
			 select ((select count(*) - 10 from friendship f where follower_accounts_id = a1.id and date_created between :startDate and :endDate) / (select count(*) from friendship f where follower_accounts_id = a1.id)) as count from accounts a1 where a1.id = a.id
			) * ${SPECTATOR_WEIGHT_FRIENDSHIP} as spectatorScore3,
			LOG10(
				(select sum(e.like_count)/count(e.id) as likes_per_exp from experiences e where e.isactive = 1 and e.date_created between :startDate and :endDate and e.accounts_id = a.id group by e.accounts_id)/
				(select count(*) + 5 as followers from friendship followers where following_accounts_id = a.id and date_created <= :endDate))/:${LIKES_PER_FOLLOWER_DIVISOR_PARAM} * ${CREATOR_WEIGHT_LIKES_POWER_PER_FOLLOWER} as creatorScore1,
			LOG10((select sum(e.like_count)/count(e.id) as likes_per_exp from experiences e where e.isactive = 1 and e.date_created between :startDate and :endDate and e.accounts_id = a.id group by e.accounts_id))/:${LIKES_PER_EXP_DIVISOR_PARAM} * ${CREATOR_WEIGHT_LIKES_PER_EXPERIENCE} as creatorScore2,
			(select LOG10(count(e.id))/:${NUM_EXP_DIVISOR_PARAM} * ${CREATOR_WEIGHT_NUM_EXPERIENCES} as num_exp_score from experiences e where e.isactive = 1 and e.date_created between :startDate and :endDate and e.accounts_id = a.id) as creatorScore3,
			(
				LOG10((select count(*) as followers from friendship followers where following_accounts_id = a.id and date_created between :startDate and :endDate)/
				(COALESCE( NULLIF((select count(*) as following from friendship following where follower_accounts_id = a.id and date_created between :startDate and :endDate),0),1)))
			)/:${STAR_POWER_DIVISOR_PARAM} * ${CREATOR_WEIGHT_STAR_POWER} as creatorScore4,
			(select max(e.date_created) from experiences e where e.accounts_id = a.id) as lastPostDate
		"""

	public static final String SCORING_FROM_CLAUSE = "accounts a"

	public static final String CLOUT_INSERT_STATEMENT = "INSERT INTO `clout`(`account_id`, `creator_score`, `spectator_score`) VALUES (:accountId, :creatorScore, :spectatorScore);"

	public static final String CLOUT_UPDATE_STATEMENT = "UPDATE `clout` SET `creator_score` = :creatorScore, `spectator_score` = :spectatorScore, `last_updated` = now() WHERE `clout`.`id` = :id;"
}
