#!/bin/bash
echo "Creating apps dir"
sudo mkdir /var/lib/apps
echo "Creating apps/wadi"
sudo mkdir /var/lib/apps/wadi
echo "Copying application.yml from s3 to /var/lib/apps/wadi"
sudo aws s3 cp s3://it.yonder.deploy/workers/wadi/application.yml /var/lib/apps/wadi/
echo "Copying wadi.jar from s3 to /var/lib/apps/wadi"
sudo aws s3 cp s3://it.yonder.deploy/workers/wadi/wadi.jar /var/lib/apps/wadi/
echo "Copying wadi.cron from s3 to /etc/cron.d/wadi"
sudo aws s3 cp s3://it.yonder.deploy/workers/wadi/wadi.cron /etc/cron.d/wadi
echo "Copying wadi.sh from s3 to /usr/local/bin/wadi.sh"
sudo aws s3 cp s3://it.yonder.deploy/workers/wadi/wadi.sh /usr/local/bin
echo "Making wadi.sh executable"
sudo chmod 755 /usr/local/bin/wadi.sh