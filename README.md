#Wadi (Worker Process)
######The Wadi are a humanoid species that were discovered by the Vulcans. They are obsessed with games and gambling.
## Purpose 
We are trying on a large scale to create a personalization and recommendation engine. This is a very complicated and 
involved process that will take much time to accomplish. In the interim, Wadi will serve as a batch processor for a more 
simple scoring calculator. Each user will receive a creator score and a spectator score, computed by Wadi. These scores are based on a number of social factors including the number of posts, likes per post, number of followers, number following, number of likes given, and number of comments written over a certain time period (i.e. one month). The scores will then be used by our Private API to recommend users that are a good match for a registering user.

## The Project
A Spring Boot application running Spring Batch to process our user information and score each user.

## Technologies
Spring Boot, Spring Batch, Groovy, MySQL and cron

## Building Wadi
A simple `gradle build` will do the trick. The .jar file will be output to build/libs/wadi.jar.

**Note: When building the project in intellij, the test in AccountScoringDataRowMapperSpec will fail because of a mocking 
error unless you go to Project Settings > Libraries > + Library > Java > Looks in your .m2 for org.objenesis and
choose a version >= 1.2.0. Running on the command line will pass without anything special.**

## Running Wadi
To run the default profile just execute the command below.

```
gradle bootRun
```

In order to run a specific profile, you need to use the following 

```
java -jar build/libs/wadi-0.1.0.jar --spring.profiles.active=staging  

OR

java -jar build/libs/wadi-0.1.0.jar --spring.profiles.active=production  
```
**Note: In order for the profile application profile and the override to work, you must be running the jar in the same directory as the application.yml file.**

The way we are running this project on servers is via crontab. The deploy the cron job to staging manually:

```
#!bash
# Copy the cron and script file that the crontab will execute. Assumes a 'stagingserver' in your ssh config.
_$ scp application.yml stagingserver:/tmp
_$ scp build/libs/wadi-<version>.jar staging server:/tmp
_$ scp wadi.cron stagingserver:/tmp
_$ scp wadi.sh stagingserver:/tmp
_$ ssh stagingserver

# Move the app files into the appropriate location
~$ sudo mv /tmp/wadi-<version>.jar /var/lib/apps/wadi/
~$ sudo mv /tmp/application.yml /var/lib/apps/wadi/
# Move the crontab file into the appropriate location
~$ sudo mv /tmp/wadi.cron /etc/cron.d/wadi
# Move the script file that crontab will run into the appropriate location
~$ sudo mv /tmp/wadi.sh /usr/local/bin
# Make the wadi.sh file executable
~$ sudo chmod +x /usr/local/bin/wadi.sh
# Make the log file
~$ sudo touch /var/log/wadilog
```